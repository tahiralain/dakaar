import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HomePage } from '../pages/home/home';
import {LoginPage} from '../pages/login/login';
import {SignupPage} from '../pages/signup/signup';
import { ForgetPage}    from '../Pages/forget/forget';
import {DashPage} from '../pages/dash/dash';
import { ViewChild } from '@angular/core';
import { Slides } from 'ionic-angular';
import { MenuPage}  from '../pages/menu/menu';
import {MainPage} from '../pages/main/main';
import {CartpagePage} from '../pages/cartpage/cartpage';
import {ProfilePage} from '../pages/profile/profile';
import {ContactPage} from '../pages/contact//contact';
import {AboutPage} from '..//pages/about/about';
import {LocationPage} from '../pages/location/location';
import {YourPage} from '../pages/your/your';
import{RiderPage} from  '../pages/rider/rider';
import{CoustomerPage} from '../pages/coustomer/coustomer';
import{FullmapPage} from '../pages/fullmap/fullmap';
import {SelectmapPage} from '../pages/selectmap/selectmap';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = HomePage;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
}

