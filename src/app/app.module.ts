import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import {LoginPage} from '../pages/login/login';
import { SignupPage} from '../pages/signup/signup';
import { ForgetPage} from '../pages/forget/forget';
import {MenuPage} from '../pages/menu/menu';
import {MainPage} from '../pages/main/main';
import {CartpagePage} from '../pages/cartpage/cartpage';
import {ProfilePage} from '../pages/profile/profile';
import {ContactPage} from '../pages/contact//contact';
import {AboutPage} from '../pages/about/about';
import {DashPage} from '../pages/dash/dash';
import {LocationPage} from '../pages/location/location';
import{YourPage} from '../pages/your/your';
import {RiderPage} from '../pages/rider/rider';
import{CoustomerPage} from '../pages/coustomer/coustomer';
import {FullmapPage} from '../pages/fullmap/fullmap';
import {SelectmapPage} from '../pages/selectmap/selectmap';
@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    SignupPage,
    DashPage,
    ForgetPage,
    MenuPage,
    MainPage,
    CartpagePage,
    ProfilePage,
    ContactPage,
    AboutPage,
    LocationPage,
    YourPage,
    RiderPage,
    CoustomerPage,
    FullmapPage,
    SelectmapPage,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp,{
      menuType:'push',
      platform:{
        android:{
          menuType:'overlay'
        }
      }
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    SignupPage,
    DashPage,
    ForgetPage,
    MenuPage,
    MainPage,
    CartpagePage,
    ProfilePage,
    ContactPage,
    AboutPage,
    LocationPage,
    YourPage, 
    RiderPage, 
    CoustomerPage, 
    FullmapPage,
    SelectmapPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {
 
}
