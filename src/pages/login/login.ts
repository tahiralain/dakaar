import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,LoadingController } from 'ionic-angular';
import {SignupPage} from '../signup/signup';
import {DashPage} from '../dash/dash';
import {ForgetPage} from '../forget/forget';


/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl:LoadingController  ) {
  }
 
 


  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }
  signup2(){
    this.navCtrl.push(SignupPage);
}
login2(){
  this.navCtrl.push(DashPage);
}
logout(){
  this.navCtrl.push(LoginPage);
}
signup3(){
  this.navCtrl.push(SignupPage);
  
}
forget(){
  this.navCtrl.push(ForgetPage );
}
presentLoading() {
  let loader = this.loadingCtrl.create({
    content: "Please wait...",
    duration: 3000,
    dismissOnPageChange: true
  });
  loader.present();
}
}

