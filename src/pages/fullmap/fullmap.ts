import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {RiderPage} from '../rider/rider';
import {AboutPage} from '../about/about';

/**
 * Generated class for the FullmapPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-fullmap',
  templateUrl: 'fullmap.html',
})
export class FullmapPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FullmapPage');
  }
  Ridepage(){
    this.navCtrl.push(RiderPage);
  }
  upcomingorder(){
    this.navCtrl.push(AboutPage);
  }
}
