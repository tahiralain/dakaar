import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {MenuPage} from '../menu/menu';
import {YourPage} from '../your/your';

/**
 * Generated class for the CartpagePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-cartpage',
  templateUrl: 'cartpage.html',
})
export class CartpagePage {

  count: number;

  constructor(public navCtrl: NavController, public navParams: NavParams) {

    this.count = 0;

  }

  addCount() {
    this.count += 1;
  }

  subtractCount() {
    if(this.count >0){
      this.count  -= 1;
    }
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CartpagePage');
  }
  Backbutton(){
    this.navCtrl.push(MenuPage)
  }
  yourpage(){
    this.navCtrl.push(YourPage)
  }
}
