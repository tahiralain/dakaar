import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CoustomerPage } from './coustomer';

@NgModule({
  declarations: [
    CoustomerPage,
  ],
  imports: [
    IonicPageModule.forChild(CoustomerPage),
  ],
})
export class CoustomerPageModule {}
