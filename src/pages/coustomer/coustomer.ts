import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {FullmapPage} from '../fullmap/fullmap';
import {AboutPage} from '../about/about';
/**
 * Generated class for the CoustomerPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-coustomer',
  templateUrl: 'coustomer.html',
})
export class CoustomerPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CoustomerPage');
  }
  Fullmap(){
    this.navCtrl.push(FullmapPage);
  }
  upcomingorder(){
    this.navCtrl.push(AboutPage);
  }
}
