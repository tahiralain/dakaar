import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {DashPage} from '../dash/dash';
import{AboutPage} from '../about/about';
import {ProfilePage} from '../profile/profile';
import {ContactPageModule} from '../contact/contact.module';
import {YourPage} from '../your/your';

/**
 * Generated class for the ContactPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html',
})
export class ContactPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ContactPage');
  }

  dash(){
    this.navCtrl.push(DashPage);
  }
  search(){
    this.navCtrl.push(ContactPage);
  }
  order(){
    this.navCtrl.push(AboutPage);
  }
  profile(){
    this.navCtrl.push(ProfilePage);
  }
  YourPage(){
    this.navCtrl.push(YourPage);
  }
}
