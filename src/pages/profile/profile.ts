import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {LoginPage} from '../login/login';
import {DashPage} from '../dash/dash'
import {ContactPage} from '../contact/contact';
import {AboutPage} from '../about/about';
/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
  }
  loginPage(){
    this.navCtrl.push(LoginPage)
  }
  Dashpage(){
    this.navCtrl.push(DashPage)
  }
  dash(){
    this.navCtrl.push(DashPage);
  }
  search(){
    this.navCtrl.push(ContactPage);
  }
  order(){
    this.navCtrl.push(AboutPage);
  }
  profile(){
    this.navCtrl.push(ProfilePage);
  }
  
}
