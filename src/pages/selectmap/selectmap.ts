import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import{AboutPage} from '../about/about';
import {CoustomerPage} from '../coustomer/coustomer';

/**
 * Generated class for the SelectmapPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-selectmap',
  templateUrl: 'selectmap.html',
})
export class SelectmapPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SelectmapPage');
  }
  upcomingorder(){
    this.navCtrl.push(AboutPage);
  }
coustomer(){
  this.navCtrl.push(CoustomerPage);
}
}
