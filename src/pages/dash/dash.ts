import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {LoginPage} from '../login/login';
import {MenuPage} from '../menu/menu';
import {ProfilePage} from '../profile/profile';
import {ContactPage} from '../contact/contact';
import {AboutPage} from '../about/about';
import {YourPage} from '../your/your';

@IonicPage()
@Component({
  selector: 'page-dash',
  templateUrl: 'dash.html',
})
export class DashPage {

  
  constructor(public navCtrl: NavController ,public navParams: NavParams){
    


  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DashPage');
  }
 logout(){
   this.navCtrl.push(LoginPage);

 }
 menu1(){
  this.navCtrl.push(MenuPage);
}
dash(){
  this.navCtrl.push(DashPage);
}
search(){
  this.navCtrl.push(ContactPage);
}
order(){
  this.navCtrl.push(AboutPage);
}
profile(){
  this.navCtrl.push(ProfilePage);
}
yourpage(){
  this.navCtrl.push(YourPage);
}

}