import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams , AlertController } from 'ionic-angular';
import {DashPage} from '../dash/dash';
import {ContactPage} from '../contact/contact';
import {ProfilePage} from '../profile/profile';
import {YourPage} from '../your/your';

 
/**
 * Generated class for the AboutPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-about',
  templateUrl: 'about.html',
})
export class AboutPage {

  pet: string = "history";

  constructor(public navCtrl: NavController, public navParams: NavParams , public alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AboutPage');
  }
  dash(){
    this.navCtrl.push(DashPage);
  }
  search(){
    this.navCtrl.push(ContactPage);
  }
  order(){
    this.navCtrl.push(AboutPage);
  }
  profile(){
    this.navCtrl.push(ProfilePage);
  }
  history(){
  
  }
  YourPage(){
    this.navCtrl.push(YourPage);
  }
}
