import { Component } from '@angular/core';
import { IonicPage, NavController, ModalController,NavParams } from 'ionic-angular';
import{MainPage} from '../main/main';
import {DashPage} from '../dash/dash';
import {YourPage} from '../your/your';

/**
 * Generated class for the MenuPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html',
})
export class MenuPage {

  constructor(public navCtrl: NavController,  public modalctrl: ModalController, public navParams: NavParams) {
  }

  launchMenuPage() {
    let modal = this.modalctrl.create(MainPage)
   modal.present();
  }
  Backbutton(){
this.navCtrl.push(DashPage);
  }
  yourpage(){
    this.navCtrl.push(YourPage);
  }
}
