import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {CartpagePage} from '../cartpage/cartpage';
import {MenuPage} from '../menu/menu';
import {YourPage} from '../your/your';
/**
 * Generated class for the MainPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-main',
  templateUrl: 'main.html',
})
export class MainPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MainPage');
  }
Cartpage(){
  this.navCtrl.push(CartpagePage);
}
menu(){
  this.navCtrl.push(MenuPage);
}
yourpage(){
  this.navCtrl.push(YourPage);
}

}
