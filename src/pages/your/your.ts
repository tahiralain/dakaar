import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ActionSheetController } from 'ionic-angular';
import {MenuPage} from '../menu/menu';
import {LocationPage} from '../location/location';
import {AboutPage} from '../about/about'; 
import {CoustomerPage} from '../coustomer/coustomer';
import {FullmapPage} from '../fullmap/fullmap';
import {SelectmapPage} from '../selectmap/selectmap';


/**
 * Generated class for the YourPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-your',
  templateUrl: 'your.html',
})
export class YourPage {
  count: number;
  count2: number;
  count3: number;
  

  constructor(public navCtrl: NavController, public navParams: NavParams,public actionSheetCtrl: ActionSheetController) {
    this.count = 1;
    this.count2= 1;
    this.count3= 1
    
      }
    
      addCount() {
        this.count += 1;
      }
    
      subtractCount() {
        if(this.count >1){
          this.count  -= 1;
        }}

        addCount2() {
          this.count2 += 1;
        }
      
        subtractCount2() {
          if(this.count2 >1){
            this.count2  -= 1;
          }}
        
          addCount3() {
            this.count3 += 1;
          }
        
          subtractCount3() {
            if(this.count3 >1){
              this.count3  -= 1;
            }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad YourPage');
  }
  Backbutton(){
    this.navCtrl.push(MenuPage)
  }
  ordernow(){
    this.navCtrl.push(LocationPage)

  }
  
  presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'SELECT DELIVERY ADDRESS',
      
      
      buttons: [
        {
          text: 'Use Current Address ',
          role: 'destructive',
          icon: 'pin',
          handler: () => {
            this.navCtrl.push(CoustomerPage)
            
          }
        },{
          text: 'Select From Map',
          icon: 'map',
          
          handler: () => {
            this.navCtrl.push(SelectmapPage)
          }
        },{
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }

  upcomingorder(){
    this.navCtrl.push(AboutPage)
  }
}

